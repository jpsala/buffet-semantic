import {inject, BindingEngine} from 'aurelia-framework';
import {ImagenesService} from './../services/imagenes';
import {Config} from './../config/config';
import {CategoriasService} from './../services/categorias';
@inject(BindingEngine, ImagenesService, CategoriasService)
export class ArticuloModel {
  constructor(bindingEngine, imagenesService, categoriasService) {
    this.bindingEngine = bindingEngine;
    this.imagenesService = imagenesService;
    this.categoriasService = categoriasService;
    this.images = this.getImages();
    this.categorias = this.getCategorias();
  }

  async getImages() {
    this.images = await this.imagenesService.getImages();
  }

  async getCategorias() {
    this.categorias = await this.categoriasService.getCategorias();
  }

  nuevo(articulo = undefined) {
    if (typeof articulo === 'undefined') {
      articulo = {
        id: -1,
        nombre: '',
        precio_venta: null,
        imagen: '',
        ratings: []
      };
    }
    return new Articulo(articulo, this.images, this.categorias, this.bindingEngine);
  }

  limpia(articulo) {
    let articuloListo = {
      id: articulo.id,
      nombre: articulo.nombre,
      precio_venta: articulo.precio_venta,
      categoria_id: articulo.categoria_id,
      imagen: articulo.imagen,
      ratings: articulo.rating
    };
    return articuloListo;
  }
}

class Articulo {
  static categorias = [];
  static images = [];

  constructor(articulo, images, categorias, bindingEngine) {
    Articulo.images = images;
    Articulo.categorias = categorias;
    this.bindingEngine = bindingEngine;
    Object.assign(this, articulo);
    this.ratingAnt = this.rating;
    this.subscriptionCategoriaId = this.bindingEngine
            .propertyObserver(this, 'categoria_id')
            .subscribe(val=> this.categoriIdChanged(val));
    // this.subscriptionRating = this.bindingEngine
    //         .propertyObserver(this, 'rating')
    //         .subscribe((val, ant)=>{
    //           if (this.ratingAnt !== undefined) {
    //             console.log('val', val, ant, this);
    //           }
    //           this.ratingAnt = val;
    //         });
  }

  async activate() {
    this.categoriasService = categoriasService;
    Articulo.categorias = await this.getCategorias();
  }

  categoriIdChanged(val) {
    this.categoria_nombre = Articulo.categorias.find(c=>Number(c.id) === Number(val)).nombre;
  }

  async getCategorias() {
    if (Articulo.categorias.length === 0) {
      Articulo.categorias = await this.categoriasService.getCategorias();
    }
  }


  get nombreConSub() {
    return ((typeof this.sub === 'undefined') ? this.nombre : `${this.nombre} ${this.sub.nombre}`);
  }

  get precioConSub() {
    return (typeof this.sub === 'undefined') ? this.precio_venta : this.sub.precio_venta;
  }

  get imagenPath() {
    return this.imagen ? `${Config.getUrlImages()}/${this.imagen.trim()}` : null;
  }

  dispose() {
    alert('dispose');
  }
}
