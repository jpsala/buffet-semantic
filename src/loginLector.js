import {inject} from 'aurelia-framework';
import AuthService from 'services/auth';
import {Router} from 'aurelia-router';
import Lector from './services/lector';
import alertify from '../node_modules/alertify.js/dist/js/alertify';
@inject(AuthService, Router, Lector)
export class LoginLector {
  socioId = '';
  checking = false;
  routeActive = false;

  constructor(auth, router, lector) {
    alertify.logPosition('top-right');
    this.auth = auth;
    this.router = router;
    this.lector = lector;
  }

  canActivate() {
    if (this.auth.loggedIn) {
      this.router.navigateToRoute('categorias');
      return false;
    }
    return true;
  }

  attached(){
    this.lector.onReadyChangedCallBack = this.onChkLectorReady.bind(this);
    this.routeActive = true;
    if (this.lector.ready){
      this.check();
    }
  }

  detached(){
    this.lector.onReadyChangedCallBack = undefined;
    this.routeActive = false;
    this.lector.cancelCmd();
    // this.lector.cmd('stop').then((a)=>{
    //   console.log('stop then', a);
    // }).catch((e)=>{
    //   console.log('stop catch', e);
    // });
    this.lector.cmdNoWait('stop');
  }

  onChkLectorReady(ready){
    if (!ready) {
      this.router.navigateToRoute('login');
    } else {
      alertify.error('El Lector está listo');
      this.check();
    }
  }

  check(){
    if (! this.routeActive){
      return;
    }
    if (!this.lector.ready){
      alertify.error('El Lector no está listo');
      return;
    }
    this.lector.cmd('check', {}, 15000).then((msg)=>{
      let encontrado = msg.data.resultado[0];
      console.log('check', msg);
      if (encontrado && Number(encontrado) === -2){
        console.log('chk timeout en servidor');
        this.checkSoon();
      } else if (encontrado && Number(encontrado) !== -1){
        // alertify.success('Detección :'.concat(msg.data.resultado[0]));
        this.socioId = msg.data.resultado[0];
        this.login();
      } else {
        console.log(msg);
        alertify.error('Detección :'.concat('No encontrado'));
        alertify.alert('No encontrado, intente nuevamente o comuníquese con la administración');
        setTimeout(()=>{$('.alertify').find('button').click();}, 5000);
        this.checkSoon();
      }
    }).catch((e)=>{
      console.log('catch:', e);
      // $('.alertify').find('button').click();
      // alertify.alert('Error', 'Error en lectura');
      // alertify.error('Error de lectura');
      // alertify.alert('Error de lectura, intente nuevamente');
      // setTimeout(()=>{$('.alertify').find('button').click();}, 5000);
      // console.log('error:', e);
      this.checkSoon(100);
    });
  }

  checkSoon(interval = 1000){
    if (!this.checking){
      this.checking = setTimeout(()=>{
        clearTimeout(this.checking);
        this.checking = undefined;
        this.check();
      }, interval);
    }
  }

  login() {
    this.auth.loginLector(this.socioId);
  }
}
