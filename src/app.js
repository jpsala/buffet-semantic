import {inject} from 'aurelia-framework';
import AuthorizeStep from './config/authorize-step';
import AuthService from './services/auth';
import {CarroService} from './services/carro';
@inject(AuthService, CarroService)

export class App {
  socket;
  constructor(auth, carroService) {
    // console.clear();
    console.info('app.constructor');
    this.carroService = carroService;
    this.auth = auth;
    this.auth.configureAuth();
  }

  configureRouter(config, router) {
    config.title = 'Aurelia';
    config.addPipelineStep('authorize', AuthorizeStep);
    let routes = [
      {route: 'categorias', name: 'categorias', moduleId: './categorias', nav: true, title: 'Categorias', auth: true},
      {route: 'menu', name: 'menu', moduleId: './menu', nav: true, title: 'Menu completo', auth: true},
      {route: 'menu/:id', name: 'menu', moduleId: './menu', nav: false, title: 'Menú', auth: true},
      {route: 'solo-carrito', name: 'carrito', moduleId: './solo-carrito', nav: false, title: 'Carrito', auth: true},
      {route: 'print', name: 'print', moduleId: './print', nav: false, title: 'Imprimir', auth: true},
      {route: 'loginLector', name: 'loginLector', moduleId: './loginLector', title: 'Login con lector', auth: false},
      {route: 'login', name: 'login', moduleId: './login', title: 'Login', auth: false},
      {route: 'admin', name: 'admin', moduleId: './admin/admin', nav: true, title: 'Admin', auth: true, admin: true},
      {route: '', redirect: 'categorias'}
    ];
    config.map(routes);


    this.router = router;
  }
}
