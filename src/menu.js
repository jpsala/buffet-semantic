import {inject} from 'aurelia-framework';
import {ArticulosService} from './services/articulos';
import {CarroService} from './services/carro';
import {CategoriasService} from './services/categorias';
import '../semantic/dist/components/sticky.min';
import throttle from 'throttle-debounce/debounce';
@inject(ArticulosService, CarroService, CategoriasService)
export class Menu {
  categoria = undefined;
  constructor(articulosService, carroService, categoriasService) {
    this.articulosService = articulosService;
    this.carroService = carroService;
    this.categoriasService = categoriasService;
  }

  async activate(categoria) {
    categoria.id = categoria.id === 'false' ? false : categoria.id;
    this.articulos = await this.articulosService.getArticulosFiltrados(categoria.id);
  }

  fillCarrito() {
    this.carroService.add(this.articulos[0]);
    this.carroService.add(this.articulos[1]);
    this.carroService.add(this.articulos[3]);
    this.carroService.add(this.articulos[4]);
    this.carroService.add(this.articulos[5]);
    this.carroService.add(this.articulos[6]);
    this.carroService.add(this.articulos[7]);
    this.carroService.add(this.articulos[0]);
    this.carroService.add(this.articulos[1]);
    this.carroService.add(this.articulos[3]);
    this.carroService.add(this.articulos[4]);
    this.carroService.add(this.articulos[5]);
    this.carroService.add(this.articulos[6]);
    this.carroService.add(this.articulos[7]);
    this.carroService.add(this.articulos[0]);
    this.carroService.add(this.articulos[1]);
    this.carroService.add(this.articulos[3]);
    this.carroService.add(this.articulos[4]);
    this.carroService.add(this.articulos[5]);
    this.carroService.add(this.articulos[6]);
    this.carroService.add(this.articulos[7]);
    this.carroService.add(this.articulos[0]);
    this.carroService.add(this.articulos[1]);
    this.carroService.add(this.articulos[3]);
    this.carroService.add(this.articulos[4]);
    this.carroService.add(this.articulos[5]);
    this.carroService.add(this.articulos[6]);
    this.carroService.add(this.articulos[7]);
    carroScrollDown();
  }

  attached() {
    chkHeightCarro();
    $(window).resize(throttle( 250, chkHeightCarro));
    $(document).scroll(throttle( 250, chkHeightCarro));
  }

}

function chkHeightCarro() {
  let $carro = $('#carro-div');
  let $navBar = $('nav-bar');
  let heightAnt = $carro.css('height');
  let marginTop = $navBar.height() + 50;
  // let marginTop = $navBar.isOnScreen() ? $navBar.height() + 30 : 10;
  let height = window.innerHeight - marginTop + 'px';
  if (height !== heightAnt) {
    $carro.css('height', height);
    carroScrollDown();
  }
}

function carroScrollDown() {
  let d = $('#carro-div');
  d.scrollTop(d.prop('scrollHeight'));
}

$.fn.isOnScreen = function() {
  let win = $(window);

  let viewport = {
    top: win.scrollTop(),
    left: win.scrollLeft()
  };
  viewport.right = viewport.left + win.width();
  viewport.bottom = viewport.top + win.height();

  let bounds = this.offset();
  bounds.right = bounds.left + this.outerWidth();
  bounds.bottom = bounds.top + this.outerHeight();

  return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};
