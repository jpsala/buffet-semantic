import {inject} from 'aurelia-framework';
import AuthService from 'services/auth';
import {Router} from 'aurelia-router';
import Lector from './services/lector';
import alertify from '../node_modules/alertify.js/dist/js/alertify';

@inject(AuthService, Router, Lector)
export class Login {
  username = '';
  password = '';

  constructor(auth, router, lector) {
    alertify.logPosition('top-right');
    this.auth = auth;
    this.router = router;
    this.lector = lector;
    this.lector.onReadyChangedCallBack = this.onChkLectorReady.bind(this);
  }

  canActivate() {
    if (this.auth.loggedIn) {
      this.router.navigateToRoute('categorias');
      return false;
    } else if (this.lector.ready) {
      this.router.navigateToRoute('loginLector');
    }
    return true;
  }

  onChkLectorReady(ready){
    if (ready){
      if (!this.auth.loggedIn){
        this.router.navigateToRoute('loginLector');
      }
    }
  }

  submit() {
    this.auth.login(this.username, this.password);

      // .fetch('oauth/v2/token', {
      //   method: 'post',
      //   headers: {
      //     'Authorization': 'Basic MV8ydDRycXFxeHI2bzA4MDBnOGtnc3NnZ3Mwb3dzd2tra29zMGtrd28wc3cwbzhva3c4Yzo1eXVjZ2NlbmJuazBnYzBrMDBrc2dnY3djY3djNDBzZzBnb2Nzc2Njd2dnODB3dzRrYw'
      //   },
      //   body: json({
      //     grant_type: 'password',
      //     username: this.username,
      //     password: this.password
      //   })
      // })
      // .then((response) => {
      //   if (response.status === 200) {
      //     return response.json();
      //   }
      //
      //   let err = new Error(response.statusText);
      //   err.response = response;
      //   throw err;
      // })
      // .then(response => console.log(response))
      // .catch((err) => {
      // });
  }
}
