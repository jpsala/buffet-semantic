/*eslint-disable no-var,no-unused-vars*/
// var Promise = require('bluebird'); // Promise polyfill for IE11

import { bootstrap } from 'aurelia-bootstrapper-webpack';
import '../styles/styles.css';

bootstrap(function(aurelia) {
  if (document.location.hostname === 'localhost') {
    aurelia.use
      .standardConfiguration()
      .developmentLogging();
  } else {
    aurelia.use
      .standardConfiguration();
  }
  aurelia.start().then(() => aurelia.setRoot('app', document.body));
});
