import {inject} from 'aurelia-framework';
import alertify from '../../node_modules/alertify.js/dist/js/alertify';
import {alertifyClose} from '../util';
import {Config} from '../config/config.js';
import Lector from '../services/lector';
import '../../semantic/dist/components/search.min';
import '../../semantic/dist/components/api';
import '../../semantic/dist/components/dropdown';
import '../../semantic/dist/semantic.min';
@inject(Lector, Config)
export default class LectorEnroll{
  socioId;
  constructor(lector, config){
    alertify.logPosition('top-right');
    this.lector = lector;
    this.config = config;
  }

  enroll(){
    alertify.log('Presione el dedo contra el detector de huellas tres veces durante dos segundos');
    this.lector.cmd('enroll', {'socio_id': Number(this.socioId)}, 20000).then((msg)=>{
      if (msg.data.status){
        alertify.success('OK');
      } else {
        alertify.log('Error, mala lectura');
      }
    }).catch((e)=>{
      alertify.error('Error, timeout');
    });
  }

  deleteTemplates(){
    alertify.confirm('Estás por borrar todas las huellas de la base de datos, seguro?', () => {
      this.lector.cmd('deleteTemplates');
    });
  }

  attached(){
    let that = this;

    $(document).ready(()=>{
      $('#alumno')
        .search({
          apiSettings: {
            mockResponseAsync: this.getsocios.bind(this)
          },
          minCharacters: 2,
          maxResults: 10,
          showNoResults: false,
          searchDelay: 500,
          onSelect(result, response){
            that.socioId = result.id;
          }
        });
    });
  }

  async getsocios(settings, callback){
    let query = $('#alumno').search('get value');
    console.log(await this.SociosService.getsocios(query, 10));
    callback({results: await this.SociosService.getsocios(query, 10)});
  }

  getValue(){
    console.log('ok');
    let a = $('.ui.search').search('get value');
    console.log(a);
    console.log($('.ui.search')
      .search('get result', a));
  }

  check(){
    // let alert = alertify.alert('Atención', 'Presione el dedo contra el detector de huellas hasta recibir un mensaje');
    alertify.alert('Presione el dedo contra el detector de huellas hasta recibir un mensaje');
    this.lector.cmd('check', {}, 10000).then((msg)=>{
      let encontrado = msg.data.resultado[0];
      console.log(msg.data && msg.data.resultado[0]);
      alertifyClose(2000);
      if (encontrado && Number(encontrado) !== -1){
        alertify.alert('Detección :'.concat(msg.data.resultado[0]));
        alertify.success('Detección :'.concat(msg.data.resultado[0]));
      } else {
        alertify.alert('Error', 'Detección :'.concat('No encontrado'));
        alertify.error('Detección :'.concat('No encontrado'));
      }
    }).catch((e)=>{
      $('.alertify').find('button').click();
      alertify.alert('Error', 'Error en lectura');
      alertify.error('Error en lectura');
      console.log('error:', e);
    });
  }

}
