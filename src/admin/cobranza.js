import { computedFrom, inject } from 'aurelia-framework';
import { observable } from 'aurelia-binding';
import { SociosService } from '../services/socios';
import alertify from '../../node_modules/alertify.js/dist/js/alertify'; // import toastr from 'toastr';
// eslint-disable-next-line no-unused-vars
import kalendae from '../../node_modules/kalendae/build/kalendae.min'; // import toastr from 'toastr';

// import moment from '../../node_modules/moment/min/moment.min';
// import '../../node_modules/moment/locale/es';
import moment from 'moment';
@inject(SociosService)
export class Cobranza {
  saldo;
  saldoAnt;
  resto;
  credito;
  skipCargaValores = false;
  docActivo;
  datosSocio;
  @observable cta = [];
  @observable fecha;
  @observable socioId;
  @observable pago;

  constructor(sociosService) {
    moment.locale('es');
    console.log('cobranza');
    console.log(moment().format('LLLL'));
    this.SociosService = sociosService;
  }

  attached() {
    console.log('attached');
    setTimeout(() => {
      $('#socio-search').focus();
    }, 500);
    $('#fecha')
      .kalendae({
        format: 'DD/MM/YYYY',
        autoClose: true,
        selected: this.fecha,
        subscribe: {
          'change': (val) => {
            if (this.fecha !== val.format('DD/MM/YYYY')) {
              $('#fecha').focus();
              $('.kalendae').css('display', 'none');
            }
          }
        }
      });
  }

  pagoChanged() {
    console.log('pagoChanged');
    this.resto = this.pago - this.saldo;
  }

  fechaChanged() {
    console.log('fechaChanged');
    this.cargaValores();
  }

  ctaChanged(val){
    console.log('ctaChanged', val);
  }

  @computedFrom('cta')
  get ctaParaMostrar() {
    console.log('ctaParaMostrar', this.cta);
    let total = 0;
    return this.cta.map((c) => {
      let r = c;
      total += Number(c.total);
      r.saldoArrastre = total;
      return r;
    });
  }

  async socioIdChanged(socioId) {
    console.log('socioIdChanged');
    this.resetProperties();
    // this.cargaValores();
    this.datosSocio = await this.SociosService.getDatosSocio(this.socioId);
  }

  async cargaValores() {
    if (!this.socioId) return;
    if (this.skipCargaValores) {
      /*
      En el caso en el que no se mande una fecha a this.SociosService.getCta,
      este trae una fecha (el select usa un limit de N, toma la fecha del primer registro y la devuelve)
      y nuesra fecha pasa a ser esa pero no hace falta pedir otra vez los datos ya que los datos
      ya vienen desde esa fecha
      */
      this.skipCargaValores = false;
      return;
    }
    console.log('cargaValores');
    let fecha = this.fecha ? moment(this.fecha, 'DD/MM/YYYY').format('YYYY/M/D') : undefined;
    // Si hay fecha is es inválida no hago nada
    if (fecha) {
      let fechaMoment = moment(fecha, 'YYYY/M/D');
      if ((!fechaMoment.isValid() || fechaMoment.year() < (moment().year() - 100))) {
        return;
      }
    }
    let resp = await this.SociosService.getCta(this.socioId, fecha);
    this.cta = resp.data;
    this.saldo = Number(resp.saldo);
    this.saldoAnt = Number(resp.saldoAnt);
    this.credito = Number(resp.credito);
    this.pago = this.saldo > 0 ? this.saldo : 0;
    if (resp.fecha) {
      // ver comentario en el if de if (this.skipCargaValores)
      this.skipCargaValores = true;
      this.fecha = moment(resp.fecha, 'YYYY-MM-DD').format('DD/MM/YYYY');
    }
    setTimeout(() => { $('#pago').focus().select(); }, 500);
  }

  async registraPago() {
    let resp;
    try {
      resp = await this.SociosService.registraPago(this.socioId, this.resto, this.cta, this.pago);
    } catch (error) {
      alertify.alert('Error registrando el pago:' + error);
    }
    if (resp.status === 200) {
      alertify.success('El pago fué registrado');
      setTimeout(() => {
        this.socioId = undefined;
        $('#alumno').search('set value', '');
        setTimeout(() => { $('#socio-search').focus(); }, 500);
      }, 1000);
    } else {
      alertify.alert('Error registrando el pago:' + resp.status);
    }
  }

  async aplica() {
    let resp;
    try {
      resp = await this.SociosService.aplica(this.socioId);
    } catch (error) {
      alertify.alert('Error registrando el pago:' + error);
    }
    if (resp.status === 200) {
      alertify.info('Aplicación registrada', 'Info');
      this.cargaValores();
    } else {
      alertify.alert('Error registrando el pago:' + resp.status);
      console.log(resp);
    }
  }

  getDocDet(doc, event) {
    return this.SociosService.getDocDet(doc.id).then((det) => {
      this.docActivo = doc;
      this.docActivo.det = det.data;
    });
  }

  resetProperties() {
    console.log('resetProperties');
    this.cta = [];
    this.saldo = undefined;
    this.saldoAnt = undefined;
    this.resto = undefined;
    this.credito = undefined;
    this.skipCargaValores = false;
    this.docActivo = undefined;
    this.fecha = undefined;
    this.pago = undefined;
  }

}
