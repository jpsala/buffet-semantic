export class Admin {
  configureRouter(config, router) {
    config.title = 'Aurelia';
    let routes = [
      {route: ['', 'categorias'], name: 'categorias', moduleId: './categorias', nav: true, title: 'Categorias', auth: true},
      {route: 'articulos', name: 'articulos', moduleId: './articulos', nav: true, title: 'Articulos', auth: true},
      {route: 'lectorEnroll', name: 'lectorEnroll', moduleId: './lectorEnroll', title: 'Lector Enroll', nav: true, auth: false, admin: true},
      {route: 'cobranza', name: 'cobranza', moduleId: './cobranza', nav: true, title: 'Cobranza', auth: true}
    ];
    config.map(routes);
    this.router = router;
  }
}
