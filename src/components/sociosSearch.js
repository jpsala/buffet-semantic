import {inject} from 'aurelia-framework';
import {bindable, bindingMode} from 'aurelia-framework';
import {SociosService} from '../services/socios';
import '../../semantic/dist/components/search.min';
import '../../semantic/dist/components/api';
import '../../semantic/dist/components/dropdown';
import '../../semantic/dist/semantic.min';
@inject(SociosService)
export class sociosSearch{
  @bindable({ defaultBindingMode: bindingMode.twoWay }) socioId = undefined;
  // @bindable socioId;
  @bindable conSaldo = false;
  @bindable minCharacters = 2;
  @bindable maxResults = 10;
  @bindable width = '91%';
  constructor(sociosService){
    this.socioId;
    this.SociosService = sociosService;
  }

  clearSocioId(){
    $('#alumno').search('set value', '');
    this.socioId = undefined;
    console.log('ok');
  }

  attached(){
    let that = this;
    $(document).ready(()=>{
      $('#alumno')
        .search({
          apiSettings: {
            mockResponseAsync: that.getsocios.bind(this)
          },
          minCharacters: that.minCharacters,
          maxResults: that.maxResults,
          showNoResults: false,
          searchDelay: 500,
          onSelect(result, response){
            that.socioId = result.id;
          }
        });
    });
  }

  async getsocios(settings, callback){
    let query = $('#alumno').search('get value');
    const showOnlyWidthSaldo = false;
    const limit = 10;
    callback({results: await this.SociosService.getsocios(query, limit, showOnlyWidthSaldo)});
  }

}
