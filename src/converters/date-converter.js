import moment from 'moment';
export class DateFormatValueConverter {
  toView(value, inputFormat = ['DD/MM/YYYY', 'YYYY/MM/DD'], outputFormat = 'DD/MM/YYYY') {
    moment.locale('es');
    return moment(value, inputFormat).format(outputFormat);
  }
}
