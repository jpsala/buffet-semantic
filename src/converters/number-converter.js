import numbro from 'numbro';
export class NumberFormatValueConverter {
  toView(value, format = '$0,0.00') {
    if (!value){
      return null;
    }
    return numbro(value).format(format);
  }
}
