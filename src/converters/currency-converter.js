import numbro from 'numbro';
export class CurrencyFormatValueConverter {
  toView(value = '', format = '$0,0.00') {
    if (value === '') return '';
    if (isNaN(value)) value = 0;
    return numbro(value).format(format);
  }
}
