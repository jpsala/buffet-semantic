import {HttpClient, json} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';
import AuthService from '../services/auth';
@inject(HttpClient, AuthService)
export class SociosService {
  constructor(http, authService) {
    this.http = http;
    this.authService = authService;
  }

  async getsocios(query, limit = 10, conSaldo = false) {
    return await this
      .http
      .fetch('/socios', {
        method: 'post',
        body: json({query: query, limit: limit, con_saldo: conSaldo})
      }).then((r)=>{
        console.log(r.select);
        return r.data;
      });
  }

  selectizeLoad = function(query, callback) {
    if (!query.length) return callback();
    this.getSocios(query, true).then(r=> {
      callback(r);
    });
  }.bind(this);


  getDatosSocio(socioId) {
    if (!socioId) return;
    return this.http.fetch('/datosSocio', {
      method: 'POST',
      body: JSON.stringify({
        socio_id: socioId
      })
    })
      .then(resp=>resp.data)
      .then(data=>{
        data.password_buffet2 = data.password_buffet;// = "";
        return data;
      });
  }

  getCta(socioId, fecha) {
    return this
      .http
      .fetch('/cta', {
        method: 'POST',
        body: json({socio_id: socioId, fecha: fecha})
      });
  }

  getDocDet(docId) {
    return this
      .http
      .fetch('/ctaDet', {
        method: 'POST',
        body: json({doc_id: docId})
      });
  }

  async getNCs(socioId) {
    return this
      .http
      .fetch('/ncs', {
        method: 'POST',
        body: json({socio_id: socioId})
      }).then(resp=>resp.data);
  }

  registraPago(socioId, resto, cta, pago) {
    return this.http.fetch('/pago', {
      method: 'POST',
      body: json({socio_id: socioId, cta: cta, resto: resto, pago: pago})
    });
  }

  aplica(socioId) {
    return this.http.fetch('/aplica', {
      method: 'POST',
      body: json({socio_id: socioId})
    });
  }
}
