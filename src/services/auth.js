// import {EventAggregator} from 'aurelia-event-aggregator';
import {inject, computedFrom} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {HttpClient, json} from 'aurelia-fetch-client';
import {Config} from '../config/config.js';
// import $ from 'jquery';
const AUTH_TOKEN_NAME = 'ml-auth-token';
@inject(HttpClient, Router, Config)
export default class AuthService {
  _user = {'nombre': '', apellido: '', id: undefined};

  constructor(http, router, config) {
    this.config = config;
    this.http = http;
    this.router = router;
    this.storage = window.localStorage;
    let userData = this.storage.getItem('user');
    this._user = typeof userData !== 'undefined' ? JSON.parse(this.storage.getItem('user')) : {};
  }

  configureAuth() {
    let that = this;
    this.http.configure(config => {
      config
                .withBaseUrl(Config.getUrlApi())
                .withDefaults({
                  headers: {
                    'Content-Type': 'application/json',
                    'credentials': 'include',
                    'withCredentials': true
                  },
                  credentials: 'include',
                  method: 'post',
                  crossDomain: true
                })
                .withInterceptor({
                  request(request) {
                    if (that.loggedIn) {
                      request.headers.append('authorization', `${that.getToken()}`);
                    }
                    return request;
                  },
                  response(r) {
                    return r.json()
                            .then((response) => {
                                //let ruta = that.router;
                                //console.log('api-interceptor-response',response,that.router.currentInstruction);
                                //console.log('api-interceptor-response',ruta,response);
                              if (response.status === 401) {
                                    //let ruta = that.router.currentInstruction;//.config.name;
                                    //console.log('401 en interceptor, ruta:', ruta, 'response:', response);
                                that.removeToken();
                                // alert('No autorizado');
                                console.error('No autorizado %O', response);
                                that.router.navigateToRoute('login');
                                throw new Error('No autorizado!!!');
                              }
                              return response;
                            });
                  }
                });
    });
  }

  @computedFrom('_user')
  get user() {
    return this._user;
  }

  set user(user) {
    this._user = user;
  }

  routeAuthorized(route) {
    if (!this.loggedIn || (route.admin && !this.user.isAdmin)) {
      return false;
    }
    return true;
  }

  login(username, password) {
    this
            .http
            .fetch('/auth', {
              body: json({grant_type: 'password', username: username, password: password})
            })
            .then((response) => {
              if (response.status === 200) {
                return response;
              }
              throw new Error(response.statusText);
            })
            .then((response) => {
              let token = response.access_token;
                //console.log('login token %O',token);
              this.saveToken(token);
              this.user = Object.assign({}, response.user);
              delete this.user.is_admin;
              this.user.isAdmin = response.user.is_admin === '1';
              this.storage.setItem('user', JSON.stringify(this.user));
                //console.log(this.router.navigateToRoute);
              this.router.refreshNavigation();
              this.router.navigateToRoute('categorias');
            })
            .catch((err) => {
              console.log(err);
              // alert(err);
            });
  }

  logout() {
    if (!this.loggedIn) {
            //this.closeThisWindow();
    }
    this.http
            .fetch('/logout')
            .then((response) => {
              this.storage.removeItem('user');
              this.user = {'nombre': '', 'apellido': ''};
              this.removeToken();
              // this.router.refreshNavigation();
              this.router.navigateToRoute('login');
              return response;
            })
            .catch((err) => {
              alert(err);
            });
  }

  loginLector(socioId) {
    this
            .http
            .fetch('/authLector', {
              body: json({grant_type: 'password', socio_id: socioId})
            })
            .then((response) => {
              if (response.status === 200) {
                return response;
              }
              throw new Error(response.statusText);
            })
            .then((response) => {
              let token = response.access_token;
                //console.log('login token %O',token);
              this.saveToken(token);
              this.user = Object.assign({}, response.user);
              delete this.user.is_admin;
              this.user.isAdmin = response.user.is_admin === '1';
              this.storage.setItem('user', JSON.stringify(this.user));
                //console.log(this.router.navigateToRoute);
              this.router.refreshNavigation();
              this.router.navigateToRoute('categorias');
            })
            .catch((err) => {
              console.log(err);
              // alert(err);
            });
  }

  get loggedIn() {
    return this.getToken() !== null;
  }

  getToken() {
    return this.storage.getItem(AUTH_TOKEN_NAME);
  }

  saveToken(token) {
    this.storage.setItem(AUTH_TOKEN_NAME, token);
  }

  removeToken() {
    this.storage.removeItem(AUTH_TOKEN_NAME);
  }

  closeThisWindow() {
    if (!(typeof process === 'undefined') && process.versions.electron) {
        // let BrowserWindow = require('electron').remote.BrowserWindow;
        // this.win = BrowserWindow.getFocusedWindow();
        // this.win.close();
    }
  }
}
