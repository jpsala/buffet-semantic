import { HttpClient } from 'aurelia-fetch-client';
import { inject, observable } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Config } from '../config/config';
@inject(EventAggregator, HttpClient)
export class Socket {
  @observable actualRouterRouteName;
  @observable autoReconnect;
  socket = undefined;
  connecting = true;
  connected = false;
  messages = [];
  reconnectIntervalHandler = undefined;
  routesWhenLectorConnected = ['/admin/lectorEnroll', '/login', '/admin'];
  constructor(ea, http) {
    this.ea = ea;
    this.http = http;
    // console.log(this.getUrlWebSocketServer());
    this.serverAddress = 'ws://192.168.1.175:8000';
    this.connect();
    this.routeChangheSubscription = this.ea.subscribe(
      'router:navigation:success', (r) => {
        this.actualRouterRouteName = r.instruction.fragment;
      }
    );
  }

  actualRouterRouteNameChanged(routeName) {
    console.log('ruta', routeName);
    this.autoReconnect = this.estaRutaUsaSocket(routeName);
  }

  estaRutaUsaSocket(routeName){
    let index = this.routesWhenLectorConnected
      .findIndex((rn) => routeName === rn);
    return index !== -1;
  }

  autoReconnectChanged(val) {
    if (val) {
      this.autoReconnectOn();
    } else {
      this.autoReconnectOff();
    }
    console.log('socket autoReconnect', val);
  }

  async getUrlWebSocketServer() {
    let urlApi = Config.getUrlApi();
    console.log(urlApi);
    this.http.fetch(urlApi)
      .then((r) => {
        console.log(r);
      });
  }

  connect() {
    return new Promise((resolve, reject) => {
      if (!this.socket || this.socket.readyState !== this.socket.CONNECTING) {
        this.connecting = true;
        this.socket = new WebSocket(this.serverAddress);
        this.socket.onopen = () => { this.onopen(resolve); };
        this.socket.onerror = () => { this.onerror(reject); };
      }
    });
  }

  onopen(resolve) {
    this.socket.onmessage = this.onmessagerecived.bind(this);
    this.socket.onclose = this.onclose.bind(this);
    this.connected = true;
    this.connecting = false;
    this.ea.publish('socket_connected');
    console.log('socket conected');
    this.autoReconnectOff();
    resolve(this.socket);
  }

  onerror(rejectPromise) {
    this.connected = false;
    this.connecting = false;
    if (this.autoReconnect) this.autoReconnectOn();
    console.log('socket connection error');
    console.log('Error de conexión');
    rejectPromise('Error de conexión');
  }

  onclose() {
    console.log('socket disconnected');
    if (this.autoReconnect) this.autoReconnectOn();
    this.connected = false;
    this.connecting = false;
    console.log('socket_disconnected');
    this.ea.publish('socket_disconnected');
  }

  autoReconnectOn() {
    if (!this.reconnectIntervalHandler) {
      this.reConnect();
      this.reconnectIntervalHandler = setInterval(() => { this.reConnect(); }, 500);
    }
  }

  autoReconnectOff() {
    if (this.reconnectIntervalHandler) {
      // console.log('autoreconnect off');
      clearInterval(this.reconnectIntervalHandler);
      this.reconnectIntervalHandler = undefined;
    }
  }

  reConnect() {
    if (this.connected || !this.autoReconnect) {
      return;
    }
    this.connect().then(() => {
      // console.log('reconnected');
    }).catch((e) => {
      // console.log('error reconnecting', e);
    });
  }

  onmessagerecived(a) {
    console.log('onmessagerecived', a.data);
    // console.log('onmessagerecived', JSON.parse(a.data).cmd || 'unknown cmd');
    this.ea.publish('socket_message', (a.data));
    this.messages.push(a.data);
  }


  send(msg) {
    if (this.connected) this.socket.send(JSON.stringify(msg));
  }

}
